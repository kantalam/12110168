﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebsiteLoLNews.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Chào mừng đến với TIN NHANH LIÊN MINH HUYỀN THOẠI - Website tổng hợp tin tức nhanh chóng, chính xác và liên tục về các sự kiện Liên Minh Huyền Thoại trên khắp thế giới";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Tải game Liên Minh Huyền Thoại";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Giải đáp thắc mắc và hỗ trợ";

            return View();
        }
        public ActionResult Result()
        {
            ViewBag.Message = "Xem kết quả thi đấu các giải đấu";

            return View();
        }
        public ActionResult Schedule()
        {
            ViewBag.Message = "Xem lịch thi đấu và kênh tường thuật các giải đấu";

            return View();
        }
    }
}
