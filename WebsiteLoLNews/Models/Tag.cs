﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteLoLNews.Models
{
    public class Tag
    {
        public int ID { set; get; }
        public String Title { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}